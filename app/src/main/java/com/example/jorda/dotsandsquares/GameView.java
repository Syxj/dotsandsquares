package com.example.jorda.dotsandsquares;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;

/**
 * Created by Jordan on 10/25/2017.
 */

public class GameView extends SurfaceView implements Runnable {

    volatile boolean playing;

    //game thread
    private Thread gameThread = null;

    //Objects used for drawing
    private Paint paint;
    private Canvas canvas;
    private SurfaceHolder surfaceHolder;
    private int backgroundColor;
    private Bitmap dot;

    //for making nodes
    private Context cont;
    private int screX;
    private int screY;

    private int leeway;

    private Theme gameTheme;
    private Board gameBoard;

    private ArrayList<Player> playerList;
    private int playerTurn = 0;

<<<<<<< HEAD
    private ArrayList<Bar> bars;

=======
>>>>>>> d93ea57feb660c08c1e63a45cf4e39a0c5364ee1
    //Class constructor
    public GameView(Context context, int screenX, int screenY) {
        super(context);

        screX = screenX;
        screY = screenY;
        cont = context;

        surfaceHolder = getHolder();
        paint = new Paint();

 //       backgroundColor = Color.rgb(255,255,255);

        leeway = 100;

        gameTheme = new Theme(context, "basic_theme",leeway);

<<<<<<< HEAD
        gameBoard = new Board(3, 3, screX, screY);
=======
        gameBoard = new Board(4, 4, screX, screY);
>>>>>>> d93ea57feb660c08c1e63a45cf4e39a0c5364ee1

        playerList = new ArrayList<Player>();
        playerList.add(new Player("One"));
        playerList.add(new Player("Two"));

        playerTurn = 0;

<<<<<<< HEAD
        bars = new ArrayList<Bar>();

=======
>>>>>>> d93ea57feb660c08c1e63a45cf4e39a0c5364ee1
        if (surfaceHolder.getSurface().isValid()) {

            canvas = surfaceHolder.lockCanvas();

            canvas.drawColor(Color.WHITE);

            surfaceHolder.unlockCanvasAndPost(canvas);
        }
    }

    @Override
    public void run() {
        while (true) {
            if (playing) {
                //update the frame
                update();

                //draw the frame
                draw();

                //control
                control();
            }
        }
    }

    private void update() {
        //do stuff
    }

    private void draw() {
        if (playing) {
            if (surfaceHolder.getSurface().isValid()) {

                canvas = surfaceHolder.lockCanvas();

                for(int i = 0; i < gameBoard.getLength(); i++)
                {
                    Node tmpNode = gameBoard.getNode(i);
                    canvas.drawBitmap(
                            gameTheme.getDot(),
                            (int)tmpNode.getX(),
                            (int)tmpNode.getY(),
                            paint);
                }

<<<<<<< HEAD
                for(int i = 0; i < bars.size(); i++)
                {
                    if(bars.get(i).getDirection() == 0)
                    {
                        canvas.drawBitmap(
                                gameTheme.getBarVertical(),
                                (int)bars.get(i).getX(),
                                (int)bars.get(i).getY(),
                                paint);
                    }
                    else
                    {
                        canvas.drawBitmap(
                                gameTheme.getBarHorizontal(),
                                (int)bars.get(i).getX(),
                                (int)bars.get(i).getY(),
                                paint);
                    }
                }

=======
>>>>>>> d93ea57feb660c08c1e63a45cf4e39a0c5364ee1
                surfaceHolder.unlockCanvasAndPost(canvas);
            }
        }
    }

    //We will update with roughly 60fps
    private void control() {
        try {
            gameThread.sleep(17);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //don't want the game to update while we are paused
    public void pause() {
        playing = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {/*nothing*/}
    }

    public void resume() {
        playing = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

    @Override
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (playing) {
            //game in progress
            Player tmpPlayer = playerList.get(playerTurn);
            double x = (double) motionEvent.getX();
            double y = (double) motionEvent.getY();
            switch(motionEvent.getAction() & MotionEvent.ACTION_MASK)
            {
                case MotionEvent.ACTION_UP:
                   // System.out.println("YOUR X: " + x + "   YOUR Y: " + y);
                    for(int i = 0; i < gameBoard.getLength(); i++)
                    {
                        Node tmpNode = gameBoard.getNode(i);
                        if ((x < tmpNode.getX() + leeway) && (x > tmpNode.getX()) && (y < tmpNode.getY() + leeway) && (y > tmpNode.getY()))
                        {
                            //pressed the dot
<<<<<<< HEAD
                            //System.out.println("NODE X: " + tmpNode.getX() + "   NODE Y: " + tmpNode.getY());
=======
                          //  System.out.println("NODE X: " + tmpNode.getX() + "   NODE Y: " + tmpNode.getY());
>>>>>>> d93ea57feb660c08c1e63a45cf4e39a0c5364ee1
                            if(tmpPlayer.getNode() == null)
                            {
                                System.out.println("PRESSED THE FIRST DOT");
                                tmpPlayer.touchedNode(tmpNode);
                                tmpPlayer.incrementTurn();
                            }
                            else
                            {
                                Node playerNode = tmpPlayer.getNode();
                                //if you touched the same node, cancel the first touch
                                if(playerNode == tmpNode)
                                {
                                    System.out.println("PRESSED THE SAME DOT");
                                    tmpPlayer.touchedNode(null);
                                    tmpPlayer.incrementTurn();
                                }
                                else {
                                    System.out.println("PRESSED THE SECOND DOT");
                                    //if not in the first row, check node above
<<<<<<< HEAD
                                    boolean nodeTouched = false;
                                    if (i >= gameBoard.getNumColumns()) {
                                        //touched the above node, node isn't already linked up
                                        if ((playerNode.getUp() == null) && (gameBoard.getBoard().indexOf(tmpNode) == (gameBoard.getBoard().indexOf(playerNode) - gameBoard.getNumColumns()))) {
                                            System.out.println("Second up");
=======
                                    if (i >= gameBoard.getNumColumns()) {
                                        //touched the above node, node isn't already linked up
                                        if ((playerNode.getUp() == null) && (gameBoard.getBoard().indexOf(tmpNode) == gameBoard.getBoard().indexOf(playerNode) - gameBoard.getNumColumns())) {
>>>>>>> d93ea57feb660c08c1e63a45cf4e39a0c5364ee1
                                            playerNode.setNode(1, tmpNode);
                                            tmpNode.setNode(2, playerNode);
                                            tmpPlayer.resetNode();
                                            tmpPlayer.incrementTurn();
                                            int scored = boxScored(tmpNode);
                                            if (scored >= 1) {
                                                playerList.get(playerTurn).increaseScore();
                                                System.out.println("Player Turn: " + playerTurn + "    Score: " + playerList.get(playerTurn).getScore());
                                            }
                                            playerTurn = (playerTurn == 0 ? 1 : 0);
<<<<<<< HEAD
                                            nodeTouched = true;
                                            Bar tmpBar = new Bar(tmpNode.getX(),tmpNode.getY()+leeway,0);
                                            bars.add(tmpBar);
                                        }
                                    }
                                    //if not in last row, check node below
                                    if ((i + gameBoard.getNumColumns()) <= (gameBoard.getLength() - 1) && !nodeTouched) {
                                        //touched the below node, node isn't already linked up
                                        if ((playerNode.getDown() == null) && (gameBoard.getBoard().indexOf(tmpNode) == gameBoard.getBoard().indexOf(playerNode) + gameBoard.getNumColumns())) {
                                            System.out.println("Second down");
=======
                                        }
                                    }
                                    //if not in last row, check node below
                                    if ((i + gameBoard.getNumColumns()) <= (gameBoard.getLength() - 1)) {
                                        //touched the below node, node isn't already linked up
                                        if ((playerNode.getDown() == null) && (gameBoard.getBoard().indexOf(tmpNode) == gameBoard.getBoard().indexOf(playerNode) + gameBoard.getNumColumns())) {
>>>>>>> d93ea57feb660c08c1e63a45cf4e39a0c5364ee1
                                            playerNode.setNode(2, tmpNode);
                                            tmpNode.setNode(1, playerNode);
                                            tmpPlayer.resetNode();
                                            tmpPlayer.incrementTurn();
                                            int scored = boxScored(tmpNode);
                                            if (scored >= 1) {
                                                playerList.get(playerTurn).increaseScore();
                                                System.out.println("Player Turn: " + playerTurn + "    Score: " + playerList.get(playerTurn).getScore());
                                            }
                                            playerTurn = (playerTurn == 0 ? 1 : 0);
<<<<<<< HEAD
                                            nodeTouched = true;
                                            Bar tmpBar = new Bar(playerNode.getX(),playerNode.getY()+leeway,0);
                                            bars.add(tmpBar);
                                        }
                                    }
                                    //if not in first column, check node to left
                                    if (i % gameBoard.getNumColumns() != 0 && !nodeTouched) {
                                        //touched the left node, node isn't already linked up
                                        if ((playerNode.getLeft() == null) && (gameBoard.getBoard().indexOf(tmpNode) == gameBoard.getBoard().indexOf(playerNode) - 1)) {
                                            System.out.println("Second left");
=======
                                        }
                                    }
                                    //if not in first column, check node to left
                                    if (i % gameBoard.getNumColumns() != 0) {
                                        //touched the left node, node isn't already linked up
                                        if ((playerNode.getLeft() == null) && (gameBoard.getBoard().indexOf(tmpNode) == gameBoard.getBoard().indexOf(playerNode) - 1)) {
>>>>>>> d93ea57feb660c08c1e63a45cf4e39a0c5364ee1
                                            playerNode.setNode(3, tmpNode);
                                            tmpNode.setNode(4, playerNode);
                                            tmpPlayer.resetNode();
                                            tmpPlayer.incrementTurn();
                                            int scored = boxScored(tmpNode);
                                            if (scored >= 1) {
                                                playerList.get(playerTurn).increaseScore();
                                                System.out.println("Player Turn: " + playerTurn + "    Score: " + playerList.get(playerTurn).getScore());
                                            }
                                            playerTurn = (playerTurn == 0 ? 1 : 0);
<<<<<<< HEAD
                                            nodeTouched = true;
                                            Bar tmpBar = new Bar(tmpNode.getX()+leeway,tmpNode.getY(),1);
                                            bars.add(tmpBar);
                                        }
                                    }
                                    //if not in last column, check node to right
                                    if ((i + 1) % gameBoard.getNumColumns() != 0 && !nodeTouched) {
                                        //touched the right node, node isn't already linked up
                                        if ((playerNode.getRight() == null) && (gameBoard.getBoard().indexOf(tmpNode) == gameBoard.getBoard().indexOf(playerNode) + 1)) {
                                            System.out.println("Second right");
=======
                                        }
                                    }
                                    //if not in last column, check node to right
                                    if ((i + 1) % gameBoard.getNumColumns() != 0) {
                                        //touched the right node, node isn't already linked up
                                        if ((playerNode.getRight() == null) && (gameBoard.getBoard().indexOf(tmpNode) == gameBoard.getBoard().indexOf(playerNode) + 1)) {
>>>>>>> d93ea57feb660c08c1e63a45cf4e39a0c5364ee1
                                            playerNode.setNode(4, tmpNode);
                                            tmpNode.setNode(3, playerNode);
                                            tmpPlayer.resetNode();
                                            tmpPlayer.incrementTurn();
                                            int scored = boxScored(tmpNode);
                                            if (scored >= 1) {
                                                playerList.get(playerTurn).increaseScore();
                                                System.out.println("Player Turn: " + playerTurn + "    Score: " + playerList.get(playerTurn).getScore());
                                            }
                                            playerTurn = (playerTurn == 0 ? 1 : 0);
<<<<<<< HEAD
                                            nodeTouched = true;
                                            Bar tmpBar = new Bar(playerNode.getX()+leeway,playerNode.getY(),1);
                                            bars.add(tmpBar);
                                        }
                                    }
                                    if(!nodeTouched)
                                    {
                                        //didn't touch a neighboring node. New node is set
                                        tmpPlayer.touchedNode(tmpNode);
                                    }

                                    if(gameOverCheck())
                                    {
                                        int oneScore = playerList.get(0).getScore();
                                        int twoScore = playerList.get(1).getScore();
                                        System.out.println("GAME OVER. PLAYER ONE: " + oneScore + "   PLAYER TWO: " + twoScore);
                                    }
=======
                                        }
                                    }
>>>>>>> d93ea57feb660c08c1e63a45cf4e39a0c5364ee1
                                }
                            }
                        }
                    }
<<<<<<< HEAD

=======
>>>>>>> d93ea57feb660c08c1e63a45cf4e39a0c5364ee1
                    break;

                case MotionEvent.ACTION_DOWN:
                    //pressed
                    break;
            }
        }
        return true;
    }

    public int boxScored(Node baseNode)
    {
        //check, from base node, each direction
        //check top boxes
        Node topNode = baseNode.getNode(1);
        Node bottomNode = baseNode.getNode(2);
        if(topNode != null)
        {
            //check left
            if(topNode.getNode(3) != null && topNode.getNode(3).getNode(2) != null && topNode.getNode(3).getNode(2).getNode(4) == baseNode)
            {
                //player got point, top left box
                if(baseNode.getBox(1) ) {return -1;}
                baseNode.setBox(1);
                topNode.setBox(3);
                topNode.getNode(3).setBox(4);
                topNode.getNode(3).getNode(2).setBox(2);

                return 2;
            }
            else if(topNode.getNode(4) != null && topNode.getNode(4).getNode(2) != null && topNode.getNode(4).getNode(2).getNode(3) == baseNode)
            {
                //player got point, top right box
                if(baseNode.getBox(2) ) {return -1;}
                baseNode.setBox(2);
                topNode.setBox(4);
                topNode.getNode(4).setBox(3);
                topNode.getNode(4).getNode(2).setBox(1);
                return 1;
            }
        }
        else if (bottomNode != null)
        {
            if(bottomNode.getNode(3) != null && bottomNode.getNode(3).getNode(2) != null && bottomNode.getNode(3).getNode(2).getNode(4) == baseNode)
            {
                //player got point, bottom left box
                if(baseNode.getBox(3) ) {return -1;}
                baseNode.setBox(3);
                bottomNode.setBox(1);
                bottomNode.getNode(3).setBox(2);
                bottomNode.getNode(3).getNode(2).setBox(4);
                return 3;
            }
            else if(bottomNode.getNode(4) != null && bottomNode.getNode(4).getNode(2) != null && bottomNode.getNode(4).getNode(2).getNode(3) == baseNode)
            {
                //player got point, bottom right box
                if(baseNode.getBox(4) ) {return -1;}
                baseNode.setBox(4);
                bottomNode.setBox(2);
                bottomNode.getNode(4).setBox(1);
                bottomNode.getNode(4).getNode(2).setBox(3);
                return 4;
            }
        }
        //no one scored
        return 0;
    }

<<<<<<< HEAD
    public boolean gameOverCheck()
    {
        return playerList.get(0).getScore() + playerList.get(1).getScore() == gameBoard.getNumBoxes();
    }

=======
>>>>>>> d93ea57feb660c08c1e63a45cf4e39a0c5364ee1
}
