package com.example.jorda.dotsandsquares;

/**
 * Created by jorda on 12/9/2017.
 */

public class Bar {

    private double startX;
    private double startY;

    private int direction;

    public Bar(double x, double y, int dir)
    {
        startX = x;

        startY = y;

        //0 == vertical, 1 == horizontal
        direction = dir;
    }

    public double getX() { return startX; }
    public double getY() { return startY; }

    public int getDirection()
    {
        return direction;
    }

}
