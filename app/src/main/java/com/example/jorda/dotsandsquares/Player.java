package com.example.jorda.dotsandsquares;

/**
 * Created by jorda on 11/18/2017.
 */

public class Player {

    private String playerName;
    private int score = 0;
    private int turnCount = 0;

    private Node lastTouched = null;

    public Player(String name)
    {
        playerName = name;
    }

    public int incrementTurn()
    {
        turnCount = (turnCount == 0 ? 1 : 0);
        return turnCount;
    }

    public void touchedNode(Node nd)
    {
        lastTouched = nd;
    }

    public void resetNode()
    {
        lastTouched = null;
    }

    public Node getNode()
    {
        return lastTouched;
    }

    public void increaseScore() { score++;}

    public int getScore() {return score;}
}
