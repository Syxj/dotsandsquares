package com.example.jorda.dotsandsquares;

/**
 * Created by Jordan on 10/25/2017.
 */

public class Node {

    //node's coordinates
    private double x;
    private double y;

    //node's neighbors
    private Node up;
    private Node down;
    private Node left;
    private Node right;

    //node's score boxes
    private boolean topLeft;
    private boolean topRight;
    private boolean bottomLeft;
    private boolean bottomRight;

    //constructor
    public Node(double xCoord, double yCoord)
    {
        x = xCoord;
        y = yCoord;
        up = null;
        down = null;
        left = null;
        right = null;
        topLeft = false;
        topRight = false;
        bottomLeft = false;
        bottomRight = false;
    }

    public Node getNode(int x)
    {
        switch(x)
        {
            case 1: return up;
            case 2: return down;
            case 3: return left;
            case 4: return right;
            default: return null;
        }
    }

    public void setNode(int x, Node tmpNode)
    {
        switch(x)
        {
            case 1:
                up = tmpNode;
                break;
            case 2:
                down = tmpNode;
                break;
            case 3:
                left = tmpNode;
                break;
            case 4:
                right = tmpNode;
                break;
        }
    }

    public void setBox(int x)
    {
        switch(x)
        {
            case 1:
                topLeft = true;
                break;
            case 2:
                topRight = true;
                break;
            case 3:
                bottomLeft = true;
                break;
            case 4:
                bottomRight = true;
                break;
        }
    }

    public boolean getBox(int x)
    {
        switch(x)
        {
            case 1: return topLeft;
            case 2: return topRight;
            case 3: return bottomLeft;
            case 4: return bottomRight;
        }
        //just in case a bad value is entered, return true so nothing screwy with points happens
        return true;
    }

    public double getX()
    {
        return x;
    }

    public double getY()
    {
        return y;
    }

    public Node getDown()
    {
        return down;
    }

    public Node getLeft()
    {
        return left;
    }

    public Node getRight()
    {
        return right;
    }

    public Node getUp()
    {
        return up;
    }
}
